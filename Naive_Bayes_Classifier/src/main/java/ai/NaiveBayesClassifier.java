package ai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NaiveBayesClassifier {

	public static final Integer DATASET_LENGTH = 435;
	public static final Integer DEMOCRATS_NUMBER = 267;
	public static final Integer REPUBLICANS_NUMBER = 168;

	public static String[][] data = new String[DATASET_LENGTH][17];
	public static List<List<Integer>> indexes = new ArrayList<>();
	public static int[][] results;

	public static void main(String[] args) {
		double finalAccuracy = 0;

		readData();
		generateRandomGroups();
		for (int i = 0; i < indexes.size(); i++) {
			learn(i);
			finalAccuracy += guess(i);
		}

		System.out.println("-----------------------------------------------------------------------");
		System.out.println("Average accuracy: " + Math.round((finalAccuracy / (double) 10) * 100.0) / 100.0);
	}

	private static void readData() {
		try (BufferedReader br = new BufferedReader(new FileReader(new File("house-votes-data-set.data")))) {
			int i = 0;
			String line;

			while ((line = br.readLine()) != null) {
				String[] splitLine = line.split(",");
				for (int j = 0; j < splitLine.length; j++) {
					data[i][j] = splitLine[j];
				}
				++i;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void generateRandomGroups() {
		List<Integer> pool = new ArrayList<>();

		for (int i = 0; i < DATASET_LENGTH; i++) {
			pool.add(i);
		}
		Collections.shuffle(pool);

		for (int i = 0; i < 10; i++) {
			indexes.add(new ArrayList<Integer>());
		}

		for (int i = 0; i < data.length; i++) {
			indexes.get(i % 10).add(pool.get(i));
		}
	}

	private static void learn(int i) {
		results = new int[2][32];

		for (int k = 0; k < indexes.size(); k++) {
			if (k != i) {
				for (int index : indexes.get(k)) {
					calculateResult(data[index]);
				}
			}
		}
	}

	private static void calculateResult(String[] data) {
		int row = (data[0].equals("democrat") ? 0 : 1);
		int col;

		for (int j = 1; j < data.length; j++) {

			if (!data[j].equals("?")) {
				col = (data[j].equals("y") ? 0 : 1);
				results[row][2 * (j - 1) + col]++;
			}
		}
	}

	private static double guess(int testGroupId) {
		int correct = 0;

		for (int id : indexes.get(testGroupId)) {

			double d = DEMOCRATS_NUMBER / (double) DATASET_LENGTH;
			double r = REPUBLICANS_NUMBER / (double) DATASET_LENGTH;
			String[] person = data[id];

			for (int i = 1; i < person.length; i++) {
				if (person[i].equals("y")) {
					d *= results[0][2 * (i - 1)] / (double) DEMOCRATS_NUMBER;
				} else if (person[i].equals("n")) {
					d *= results[0][2 * (i - 1) + 1] / (double) DEMOCRATS_NUMBER;
				}
			}

			for (int i = 1; i < person.length; i++) {
				if (person[i].equals("y")) {
					r *= results[1][2 * (i - 1)] / (double) REPUBLICANS_NUMBER;
				} else if (person[i].equals("n")) {
					r *= results[1][2 * (i - 1) + 1] / (double) REPUBLICANS_NUMBER;
				}
			}

			String result = (d > r ? "democrat" : "republican");
			if (result.equals(person[0])) {
				correct++;
			}
		}

		System.out.println("Learning " + (testGroupId + 1) + " -> accuracy: " + Math.round(correct / (double) indexes.get(testGroupId).size() * 100.0) / 100.0
				+ " | wrong evaluations: " + (indexes.get(testGroupId).size() - correct));

		return correct / (double) indexes.get(testGroupId).size();
	}
}
