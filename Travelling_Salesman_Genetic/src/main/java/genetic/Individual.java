package genetic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Individual {

	private List<Integer> chromosome = new ArrayList<>();
	private int fitness;

	public Individual(final int genesNumber) {
		generateGenes(genesNumber);
		calculateFitness();
	}

	public Individual(final List<Integer> chromosome) {
		this.chromosome = chromosome;
		calculateFitness();
	}

	public Individual(final int[] chromosome) {
		for (int i : chromosome) {
			this.chromosome.add(i);
		}
		calculateFitness();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Individual) {
			Individual other = (Individual) obj;

			for (int i = 0; i < other.chromosome.size(); i++) {
				if (chromosome.get(i) != other.chromosome.get(i)) {
					return false;
				}
			}

			return true;
		}
		return false;
	}

	public void print() {
		System.out.println(chromosome + " -> " + fitness);
	}

	public int getFitness() {
		return fitness;
	}

	public int getGenesSize() {
		return chromosome.size();
	}

	public int getGene(final int i) {
		return chromosome.get(i);
	}

	public int getIndex(final int value) {
		for (int i = 0; i < chromosome.size(); i++) {
			if (chromosome.get(i) == value) {
				return i;
			}
		}

		return -1;
	}

	public void swapGenes(final int firstGene, final int secondGene) {
		Collections.swap(chromosome, firstGene, secondGene);
	}

	private void generateGenes(final int genesNumber) {
		for (int i = 0; i < genesNumber; ++i) {
			chromosome.add(i);
		}
		Collections.shuffle(chromosome);
	}

	private void calculateFitness() {
		for (int i = 0; i < chromosome.size() - 1; i++) {
			fitness += Math.abs(chromosome.get(i) - chromosome.get(i + 1));
		}
		fitness += Math.abs(chromosome.get(chromosome.size() - 1) - chromosome.get(0));
	}
}
