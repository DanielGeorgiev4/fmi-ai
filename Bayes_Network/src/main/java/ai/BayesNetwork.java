package ai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class BayesNetwork {

	public static List<List<String>> data = new ArrayList<>();
	public static List<List<Integer>> indexes = new ArrayList<>();
	public static Integer VALIDATION_GROUPS_NUMBER = 10;
	public static String[] flags = { "class", "age", "menopause", "tumor-size", "inv-nodes", "node-caps", "deg-malig",
			"breast", "breast-quad", "irradiat" };
	public static String[] flags2 = { "class", "outlook", "temp", "humidity", "windy" };

	public static void main(String[] args) {
		double finalAccuracy = 0;

		readData();
		generateRandomGroups();

		for (int i = 0; i < VALIDATION_GROUPS_NUMBER; i++) {
			finalAccuracy += test(i);
		}

		System.out.println("-----------------------------------------------------------------------");
		System.out.println(
				"Average accuracy: " + Math.round((finalAccuracy / (double) VALIDATION_GROUPS_NUMBER) * 100.0) / 100.0);
	}

	private static void readData() {
		try (BufferedReader br = new BufferedReader(new FileReader(new File("breast-cancer.data")))) {
			String line;

			while ((line = br.readLine()) != null) {
				String[] splitLine = line.split(",");
				List<String> row = new ArrayList<>();

				for (String attribute : splitLine) {
					row.add(attribute);
				}
				data.add(row);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static DecisionTree createTree(List<List<String>> learnData, String[] flags) {
		DecisionTree tree = new DecisionTree(learnData, flags);

		Stack<Node> stack = new Stack<>();
		stack.add(tree.getRoot());
		while (!stack.isEmpty()) {
			Node cur = stack.pop();

			if (cur.isLeaf()) {
				cur.setLeaf();
			} else {
				int bestGainId = bestGainAttributeId(cur.getData(), cur.getAttributes());
				if (bestGainId == -1) {
					cur.calculateClass();
				} else {
					cur.setAttributeId(bestGainId);
					List<List<List<String>>> splitData = splitByAttribute(cur.getData(), cur.getAttributeId());
					for (List<List<String>> data : splitData) {
						cur.addChild(data);
						stack.push(cur.getLastChild());
					}
				}
			}
		}

		return tree;
	}

	private static void generateRandomGroups() {
		List<Integer> pool = new ArrayList<>();

		for (int i = 0; i < data.size(); i++) {
			pool.add(i);
		}
		Collections.shuffle(pool);

		for (int i = 0; i < VALIDATION_GROUPS_NUMBER; i++) {
			indexes.add(new ArrayList<Integer>());
		}

		for (int i = 0; i < data.size(); i++) {
			indexes.get(i % VALIDATION_GROUPS_NUMBER).add(pool.get(i));
		}
	}

	private static double test(int testGroupId) {
		List<List<String>> learnGroup = new ArrayList<>();
		List<List<String>> testGroup = new ArrayList<>();

		for (int i = 0; i < indexes.size(); i++) {
			if (i == testGroupId) {
				for (Integer index : indexes.get(i)) {
					testGroup.add(data.get(index));
				}
			} else {
				for (Integer index : indexes.get(i)) {
					learnGroup.add(data.get(index));
				}
			}
		}

		System.out.print("Learning " + (testGroupId + 1) + " -> ");

		return createTree(learnGroup, flags).classifySet(testGroup);
	}

	private static double entropy(List<List<String>> data) {
		int noRecurrenceEvents = 0;
		int recurrenceEvents = 0;

		for (List<String> row : data) {
			if (row.get(0).equals("no-recurrence-events")) {
				noRecurrenceEvents++;
			} else {
				recurrenceEvents++;
			}
		}

		return ent(noRecurrenceEvents, recurrenceEvents);
	}

	private static double entropy(Map<String, Pair> data, int size) {
		double sum = 0;
		for (String key : data.keySet()) {
			Pair value = data.get(key);

			sum += value.getSum() / (double) size * ent(value.getFirst(), value.getSecond());
		}
		return sum;
	}

	private static double ent(int a, int b) {
		if (a == 0 || b == 0) {
			return 0;
		}

		return -(a / (double) (a + b)) * Math.log(a / (double) (a + b)) / Math.log(2)
				- (b / (double) (a + b)) * Math.log(b / (double) (a + b)) / Math.log(2);
	}

	private static int bestGainAttributeId(List<List<String>> data, boolean[] attributes) {
		int id = -1;
		double bestGain = -1;
		double entropy = entropy(data);

		for (int i = 1; i < attributes.length; i++) {
			if (!attributes[i]) {
				Map<String, Pair> map = new HashMap<>();

				for (List<String> row : data) {
					if (!map.containsKey(row.get(i))) {
						map.put(row.get(i), new Pair());
					}

					Pair value = map.get(row.get(i));

					if (row.get(0).equals("recurrence-events")) {
						value.incFirst();
					} else {
						value.incSecond();
					}

					map.replace(row.get(i), value);
				}

				double gain = entropy - entropy(map, data.size());
				if (bestGain < gain) {
					bestGain = gain;
					id = i;
				}
			}
		}

		return id;
	}

	private static List<List<List<String>>> splitByAttribute(List<List<String>> data, int attributeId) {
		List<List<List<String>>> result = new ArrayList<>();
		Map<String, Integer> attributes = new HashMap<>();

		for (List<String> row : data) {
			if (attributes.containsKey(row.get(attributeId))) {
				result.get(attributes.get(row.get(attributeId))).add(row);
			} else {
				attributes.put(row.get(attributeId), attributes.size());
				List<List<String>> list = new ArrayList<>();
				list.add(row);
				result.add(list);
			}
		}

		return result;
	}
}