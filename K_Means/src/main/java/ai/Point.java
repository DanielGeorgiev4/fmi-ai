package ai;

public class Point {

	private double x;
	private double y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void translate(double v) {
		x += v;
		y += v;
	}
	
	public void scale(double a, double b) {
		x = x / a * b;
		y = y / a * b;
	}
	
	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double distanceTo(Point point) {
		return Math.sqrt((x - point.x) * ((x - point.x)) + (y - point.y) * ((y - point.y)));
	}
}
