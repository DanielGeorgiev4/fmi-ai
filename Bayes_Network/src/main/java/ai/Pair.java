package ai;

public class Pair {
	
	private int x;
	private int y;
	
	public Pair() {
		
	}
	
	public void incFirst() {
		x++;
	}
	
	public void incSecond() {
		y++;
	}
	
	public int getFirst() {
		return x;
	}
	
	public int getSecond() {
		return y;
	}
	
	public int getSum() {
		return x + y;
	}
}
