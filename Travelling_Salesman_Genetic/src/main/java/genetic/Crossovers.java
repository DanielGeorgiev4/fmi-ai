package genetic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class Crossovers {

	public static List<Individual> onePoint(final Individual ind1, final Individual ind2) {
		List<Individual> result = new ArrayList<>();
		int cut = (int) (Math.random() * (ind1.getGenesSize() - 1) + 1);
		LinkedHashMap<Integer, Integer> map1 = new LinkedHashMap<Integer, Integer>();
		LinkedHashMap<Integer, Integer> map2 = new LinkedHashMap<Integer, Integer>();

		for (int i = 0; i < cut; i++) {
			map1.put(ind1.getGene(i), -1);
			map2.put(ind2.getGene(i), -1);
		}

		int id = 0;
		for (int i = cut; i < ind1.getGenesSize(); i++) {
			while (map1.containsKey(ind2.getGene(id))) {
				++id;
			}
			map1.put(ind2.getGene(id), -1);
		}

		id = 0;
		for (int i = cut; i < ind1.getGenesSize(); i++) {
			while (map2.containsKey(ind1.getGene(id))) {
				++id;
			}
			map2.put(ind1.getGene(id), -1);
		}

		result.add(new Individual(new ArrayList<>(map1.keySet())));
		result.add(new Individual(new ArrayList<>(map2.keySet())));

		return result;
	}

	public static List<Individual> partial(final Individual ind1, final Individual ind2) {
		List<Individual> result = new ArrayList<>();
		int[] arr1 = new int[ind1.getGenesSize()];
		int[] arr2 = new int[ind1.getGenesSize()];
		int frontCut = (int) (Math.random() * ind1.getGenesSize());
		int backCut = (int) (Math.random() * ind1.getGenesSize());

		Arrays.fill(arr1, -1);
		Arrays.fill(arr2, -1);

		while (backCut == frontCut) {
			backCut = (int) (Math.random() * ind1.getGenesSize());
		}
		if (frontCut > backCut) {
			int t = frontCut;
			frontCut = backCut;
			backCut = t;
		}

		for (int i = frontCut; i <= backCut; ++i) {
			arr1[i] = ind2.getGene(i);
			arr2[i] = ind1.getGene(i);
		}

		for (int i = 0; i < arr1.length; ++i) {
			if (i == frontCut) {
				i = backCut;
			} else {
				if (!contains(arr1, ind1.getGene(i))) {
					arr1[i] = ind1.getGene(i);
				} else {
					int t = ind1.getGene(i);

					while (contains(arr1, t)) {
						t = ind1.getGene(ind2.getIndex(t));
					}

					arr1[i] = t;
				}

				if (!contains(arr2, ind2.getGene(i))) {
					arr2[i] = ind2.getGene(i);
				} else {
					int t = ind2.getGene(i);

					while (contains(arr2, t)) {
						t = ind2.getGene(ind1.getIndex(t));
					}

					arr2[i] = t;
				}
			}
		}

		result.add(new Individual(arr1));
		result.add(new Individual(arr2));

		return result;
	}

	private static boolean contains(final int[] array, final int number) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == number) {
				return true;
			}
		}

		return false;
	}

}
