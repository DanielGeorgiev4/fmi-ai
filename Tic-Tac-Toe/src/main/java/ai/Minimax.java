package ai;

import java.util.List;
import java.util.Scanner;

public class Minimax {

	public static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		char[][] boardArray = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		Node board = new Node(boardArray, 0);

		System.out.print("Computer(1) or Player(2) is first? ");
		boolean isPlayerTurn = scanner.nextInt() == 2 ? true : false;
		char playerSym = isPlayerTurn ? 'X' : 'O';

		board.printBoard();
		scanner.nextLine();

		while (!board.isTerminal()) {
			if (isPlayerTurn) {
				board.makeMove(opponentMove(board), playerSym);
			} else {
				System.out.println("Computer plays:");
				if (playerSym == 'X') {
					board.makeMove(findMin(board, Integer.MIN_VALUE, Integer.MAX_VALUE).getIndexes(), 'O');
				} else {
					board.makeMove(findMax(board, Integer.MIN_VALUE, Integer.MAX_VALUE).getIndexes(), 'X');
				}
			}
			board.printBoard();
			isPlayerTurn = !isPlayerTurn;
		}
		
		board.printResult();
	}

	public static Triplet findMax(Node node, int alpha, int beta) {
		if (node.isTerminal()) {
			return new Triplet(node.value());
		}

		Triplet max = new Triplet(Integer.MIN_VALUE);
		List<Node> successors = node.generateSuccessors('X');

		for (Node successor : successors) {
			Triplet successorMin = findMin(successor, alpha, beta);
			max = successorMin.getValue() > max.getValue()
					? new Triplet(successorMin.getValue(), successor.getLastCoordinates())
					: max;
			if (max.getValue() > beta) {
				break;
			}
			alpha = max.getValue() > alpha ? max.getValue() : alpha;
		}

		return max;
	}

	public static Triplet findMin(Node node, int alpha, int beta) {
		if (node.isTerminal()) {
			return new Triplet(node.value());
		}

		Triplet min = new Triplet(Integer.MAX_VALUE);
		List<Node> successors = node.generateSuccessors('O');

		for (Node successor : successors) {
			Triplet successorMax = findMax(successor, alpha, beta);
			min = successorMax.getValue() < min.getValue()
					? new Triplet(successorMax.getValue(), successor.getLastCoordinates())
					: min;
			if (min.getValue() < alpha) {
				break;
			}
			beta = min.getValue() < beta ? min.getValue() : beta;
		}

		return min;
	}

	public static int[] opponentMove(Node board) {
		int[] move = new int[2];
		String input;

		while (true) {
			System.out.print("Enter move: ");
			input = scanner.nextLine();
			
			if (input.length() != 3 || !isIntegerInRange(input.charAt(0) - '0') || input.charAt(1) != ' '
					|| !isIntegerInRange(input.charAt(2) - '0')) {
				System.out.println("Invalid input");
			} else if (!board.isLegalMove(input.charAt(0) - '1', input.charAt(2) - '1')) {
				System.out.println("Square is already checked");
			} else {
				move[0] = input.charAt(0) - '1';
				move[1] = input.charAt(2) - '1';
				
				return move;
			}
		}
	}

	public static boolean isIntegerInRange(int n) {
		return n >= 1 && n <= 3;
	}

}
