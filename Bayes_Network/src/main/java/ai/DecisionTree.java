package ai;

import java.util.Collections;
import java.util.List;

public class DecisionTree {

	private Node root;
	private String[] attributeNames;

	public DecisionTree(List<List<String>> data, String[] attributeNames) {
		root = new Node(data, data.get(0).size());
		this.attributeNames = attributeNames;
	}

	public Node getRoot() {
		return root;
	}

	public double classifySet(List<List<String>> testData) {
		int correct = 1;

		for (int i = 0; i < testData.size(); i++) {
			if (classify(testData.get(i))) {
				correct++;
			}
		}

		System.out.println("accuracy: " + Math.round(correct / (double) testData.size() * 100.0) / 100.0
				+ " | wrong evaluations: " + (testData.size() - correct));

		return Math.round(correct / (double) testData.size() * 100.0) / 100.0;
	}

	public boolean classify(List<String> dataRow) {
		return classify(dataRow, root);
	}

	private boolean classify(List<String> dataRow, Node cur) {
		if (!cur.isLeaf()) {
			for (Node child : cur.getChildren()) {
				if (dataRow.get(cur.getAttributeId()).equals(child.getData().get(0).get(cur.getAttributeId()))) {
					return classify(dataRow, child);
				}
			}
		}
		return dataRow.get(0).equals(cur.getClassifier());
	}

	public void print() {
		System.out.println(attributeNames[root.getAttributeId()]);
		for (Node child : root.getChildren()) {
			print(child, root.getAttributeId(), 12);
		}
	}

	private void print(Node node, int parentAttributeId, int spaceCount) {
		System.out.print(String.join("", Collections.nCopies(spaceCount, " "))
				+ node.getData().get(0).get(parentAttributeId) + "|" + attributeNames[node.getAttributeId()]);

		if (node.isLeaf()) {
			System.out.println("->" + node.getClassifier());
			return;
		}
		System.out.println();
		for (Node child : node.getChildren()) {
			print(child, node.getAttributeId(), spaceCount + 16);
		}
	}
}
