package ai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Node {

	private final char[][] board;
	private int lastMove[] = { -1, -1 };
	private int depth;

	public Node(char[][] board, int depth) {
		this.board = Arrays.stream(board).map(char[]::clone).toArray(char[][]::new);
		this.depth = depth;
	}

	public void printBoard() {
		for (int i = 0; i < board.length; i++) {
			System.out.print('|');
			for (int j = 0; j < board.length; j++) {
				System.out.print(board[i][j] + "|");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public void printResult() {
		if(value() == 0) {
			System.out.println("Draw");
		} else if(value() > 0) {
			System.out.println("X wins");
		} else {
			System.out.println("O wins");
		}
	}

	public boolean isTerminal() {
		for (int i = 0; i < board.length; i++) {
			if ((board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ')
					|| (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != ' ')) {
				return true;
			}
		}

		if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
				|| (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')) {
			return true;
		}

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				if (board[i][j] == ' ') {
					return false;
				}
			}
		}

		return true;
	}

	public int value() {
		for (int i = 0; i < board.length; i++) {
			if ((board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] == 'X')
					|| (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] == 'X')) {
				return 10 - depth;
			}
			if ((board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] == 'O')
					|| (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] == 'O')) {
				return -(10 - depth);
			}
		}
		if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] == 'X')
				|| (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] == 'X')) {
			return 10 - depth;
		}
		if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] == 'O')
				|| (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] == 'O')) {
			return -(10 - depth);
		}

		return 0;
	}

	public List<Node> generateSuccessors(char sym) {
		List<Node> successors = new ArrayList<>();

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				if (board[i][j] == ' ') {
					successors.add(move(i, j, sym));
				}
			}
		}

		return successors;
	}

	private Node move(int i, int j, char sym) {
		Node node = new Node(board, depth + 1);
		node.board[i][j] = sym;
		node.lastMove[0] = i;
		node.lastMove[1] = j;

		return node;
	}

	public void makeMove(int[] move, char sym) {
		board[move[0]][move[1]] = sym;
		lastMove = move;
		depth++;
	}
	
	public boolean isLegalMove(int i, int j) {
		return board[i][j] == ' ';
	}

	public int[] getLastCoordinates() {
		return lastMove;
	}

	public int getDepth() {
		return depth;
	}
}
