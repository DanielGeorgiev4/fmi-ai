package ai;

import java.util.ArrayList;
import java.util.List;

public class Node {
	
	private List<List<String>> data;
	private boolean[] attributes;
	private int attributeId;
	private List<Node> children;
	private String classifier;
	
	public Node(List<List<String>> data, int attributeSize) {
		this.data = data;
		this.attributes = new boolean[attributeSize];
		children = new ArrayList<>();
	}
	
	public Node(List<List<String>> data, boolean[] attributes, int attributeId) {
		this.data = data;
		this.attributes = new boolean[attributes.length];
		for (int i = 0; i < attributes.length; i++) {
			this.attributes[i] = attributes[i];
		}
		this.attributes[attributeId] = true;
		children = new ArrayList<>();
		if(this.data.size() < 10) {
			calculateClass();
		}
	}
	
	public List<List<String>> getData() {
		return data;
	}
	
	public boolean[] getAttributes() {
		return attributes;
	}
	
	public String getClassifier() {
		return classifier;
	}
	
	public int getAttributeId() {
		return attributeId;
	}
	
	public List<Node> getChildren() {
		return children;
	}
	
	public void setAttributeId(int attributeId) {
		this.attributeId = attributeId;
	}
	
	public Node getLastChild() {
		return children.get(children.size() - 1);
	}
	
	public void setLeaf() {
		classifier = data.get(0).get(0);
	}
	
	public void calculateClass() {
		int noRecurrenceEvents = 0;
		int recurrenceEvents = 0;

		for (List<String> row : data) {
			if (row.get(0).equals("no-recurrence-events")) {
				noRecurrenceEvents++;
			} else {
				recurrenceEvents++;
			}
		}
		
		if(recurrenceEvents > noRecurrenceEvents) {
			classifier = "recurrence-events";
		}else {
			classifier = "no-recurrence-events";
		}
	}
	
	public void addChild(List<List<String>> data) {
		children.add(new Node(data, attributes, attributeId));
	}
	
	public boolean isLeaf() {
		if(classifier != null) {
			return true;
		}
		String firstClass = data.get(0).get(0);
		
		for (int i = 1; i < data.size(); i++) {
			if(!data.get(i).get(0).equals(firstClass)) {
				return false;
			}
		}
		
		return true;
	}
}
