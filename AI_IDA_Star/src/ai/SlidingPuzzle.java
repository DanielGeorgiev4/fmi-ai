package ai;

import java.util.PriorityQueue;

public class SlidingPuzzle {

	static int size;
	static int threshold;
	static int[][] board;
	static int spaceX;
	static int spaceY;

	static PriorityQueue<Node> openQueue = new PriorityQueue<Node>(new NodeComperator<Node>());
	static PriorityQueue<Node> visitedQueue = new PriorityQueue<Node>(new NodeComperator<Node>());

	public static void main(String[] args) {
		validateInput(args);
		Node originalState = new Node(board, null, spaceX, spaceY, 0);
		Node currentState = originalState;
		threshold = Utils.manhattanDistance(currentState.getBoard());

		while (!Utils.isSolution(currentState.getBoard())) {
			putSuccessors(currentState);
			currentState = openQueue.poll();
			if (currentState == null) {
				visitedQueue.clear();
				threshold++;
				currentState = originalState;
			}
		}

		System.out.println(currentState.getCost() + "\n");
		printSolution(currentState);
	}

	private static void validateInput(String[] args) {
		size = (int) Math.sqrt((double) Integer.valueOf(args[0]) + 1);
		board = new int[size][size];

		for (int i = 1; i < args.length; ++i) {
			board[(i - 1) / size][(i - 1) % size] = Integer.valueOf(args[i]);
			if (Integer.valueOf(args[i]) == 0) {
				spaceX = (i - 1) % size;
				spaceY = (i - 1) / size;
			}
		}
	}

	private static void putSuccessors(Node currentState) {
		if (currentState.getCost() + Utils.manhattanDistance(currentState.getBoard()) > threshold) {
			return;
		}
		Node childNode;

		visitedQueue.add(currentState);
		if (currentState.getSpaceY() != 0) {
			childNode = currentState.spaceUp();
			addUnique(childNode);
		}
		if (currentState.getSpaceY() != size - 1) {
			childNode = currentState.spaceDown();
			addUnique(childNode);
		}
		if (currentState.getSpaceX() != 0) {
			childNode = currentState.spaceLeft();
			addUnique(childNode);
		}
		if (currentState.getSpaceX() != size - 1) {
			childNode = currentState.spaceRight();
			addUnique(childNode);
		}
	}

	private static void addUnique(Node node) {
		if (!visitedQueue.contains(node) && !openQueue.contains(node)) {
			openQueue.add(node);
		}
	}

	private static void printSolution(Node node) {
		if (node.getParent() != null) {
			printSolution(node.getParent());
			System.out.println(node.getMove());
		}
	}
}