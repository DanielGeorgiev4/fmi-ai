package ai;

import java.util.Arrays;

public class Node {

	private int[][] board;
	private Node parent;
	private int spaceX;
	private int spaceY;
	private int cost;
	private String move;

	public Node(int[][] board, Node parent, int spaceX, int spaceY, int cost) {
		this.board = board;
		this.parent = parent;
		this.spaceX = spaceX;
		this.spaceY = spaceY;
		this.cost = cost;
	}

	public Node(int[][] board, Node parent, int spaceX, int spaceY, int cost, String move) {
		this(board, parent, spaceX, spaceY, cost);
		this.move = move;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Node) {
			int[][] object = ((Node) obj).getBoard();
			for (int i = 0; i < object.length; i++) {
				for (int j = 0; j < object.length; j++) {
					if (board[i][j] != object[i][j]) {
						return false;
					}
				}
			}
			return true;
		}

		return false;
	}

	public Node spaceUp() {
		return new Node(swap(spaceX, spaceY - 1), this, spaceX, spaceY - 1, cost + 1, "down");
	}

	public Node spaceDown() {
		return new Node(swap(spaceX, spaceY + 1), this, spaceX, spaceY + 1, cost + 1, "up");
	}

	public Node spaceLeft() {
		return new Node(swap(spaceX - 1, spaceY), this, spaceX - 1, spaceY, cost + 1, "right");
	}

	public Node spaceRight() {
		return new Node(swap(spaceX + 1, spaceY), this, spaceX + 1, spaceY, cost + 1, "left");
	}

	public int[][] getBoard() {
		return board;
	}

	public Node getParent() {
		return parent;
	}

	public int getSpaceX() {
		return spaceX;
	}

	public int getSpaceY() {
		return spaceY;
	}

	public int getCost() {
		return cost;
	}

	public String getMove() {
		return move;
	}

	private int[][] swap(int x, int y) {
		int[][] newBoard = Arrays.stream(board).map(int[]::clone).toArray(int[][]::new);

		newBoard[spaceY][spaceX] = newBoard[y][x];
		newBoard[y][x] = 0;

		return newBoard;
	}
}