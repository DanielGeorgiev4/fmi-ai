package ai;

public class Utils {

	public static int manhattanDistance(int[][] arr) {
		int result = 0;

		for (int i = 0; i < arr.length; ++i) {
			for (int j = 0; j < arr.length; ++j) {
				if (arr[i][j] != 0) {
					int number = arr[i][j] - 1;
					int x = number % arr.length;
					int y = number / arr.length;
					result += Math.abs(x - j) + Math.abs(y - i);
				}
			}
		}

		return result;
	}

	public static boolean isSolution(int[][] board) {
		return manhattanDistance(board) == 0;
	}
}