package genetic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.google.common.collect.MinMaxPriorityQueue;

public class Population {

	private MinMaxPriorityQueue<Individual> population;
	private List<Individual> selection = new ArrayList<>();
	private double mutationRate;

	public Population(final int citiesNumber, final int populationSize, final double mutationRate) {
		this.mutationRate = mutationRate;
		population = MinMaxPriorityQueue.orderedBy((Individual a, Individual b) -> a.getFitness() - b.getFitness())
				.create();

		for (int i = 0; i < populationSize; i++) {
			population.add(new Individual(citiesNumber));
		}
	}

	public void doSelection() {
		Iterator<Individual> iterator = population.iterator();

		while (iterator.hasNext()) {
			selection.add(iterator.next());
		}
		Collections.shuffle(selection);
		
		/*Iterator<Individual> iterator = population.iterator();
		List<Individual> list = new ArrayList<>();

		while (iterator.hasNext()) {
			list.add(iterator.next());
		}
		
		for (int i = 0; i < population.size(); i++) {
			selection.add(rouletteSelection(list));
		}*/
		
	}

	public void doCrossover() {	
		List<Individual> newGeneration;

		for (int i = 0; i < selection.size() / 2; i++) {
			newGeneration = Crossovers.partial(selection.get(i), selection.get(i + 1));

			for (Individual individual : newGeneration) {
				if (!population.contains(individual)) {
					Mutations.swap(individual, mutationRate);
					population.add(individual);
					population.removeLast();
				}
			}
		}
	}
	
	public Individual rouletteSelection(List<Individual> population) {
		Collections.shuffle(population);
	    int totalFitness = population.stream().map(Individual::getFitness).mapToInt(Integer::intValue).sum();
	    
	    Random random = new Random();
	    int selectedValue = random.nextInt(totalFitness);
	    float recValue = (float) 1/selectedValue;
	    
	    float currentSum = 0;
	    for (Individual genome : population) {
	        currentSum += (float) 1/genome.getFitness();
	        if (currentSum >= recValue) {
	            return genome;
	        }
	    }
	    
	    // In case the return didn't happen in the loop above, we just
	    // select at random
	    int selectRandom = random.nextInt(population.size());
	    return population.get(selectRandom);
	}

	public void printPopulation() {
		Iterator<Individual> iterator = population.iterator();
		while (iterator.hasNext()) {
			iterator.next().print();
		}
	}

	public void printBestIndividual() {
		population.peek().print();
	}

	public int bestIndividualFitness() {
		return population.peek().getFitness();
	}
}