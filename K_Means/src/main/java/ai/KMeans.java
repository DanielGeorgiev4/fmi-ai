package ai;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class KMeans extends JPanel {
	private static final long serialVersionUID = 1536467176400560452L;
	private static int screenSize = 800;
	private static Color[] colors = { Color.BLACK, Color.BLUE, Color.RED, Color.GREEN, Color.MAGENTA, Color.ORANGE,
			Color.PINK, Color.CYAN, Color.YELLOW, Color.GRAY, Color.LIGHT_GRAY };
	private static List<Point> data = new ArrayList<>();
	private static List<Point> clusters = new ArrayList<>();
	private static List<List<Point>> clusterizedData = new ArrayList<>();
	private static double inputDataMaxSize;
	private static int clustersNumber;

	public static void main(String[] args) {
		clustersNumber = Integer.valueOf(args[1]);
		readData(args[0]);
		createClusters();
		clusterize();
		while (recalculateClusters()) {
			clusterize();
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("kMneas");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setLocationByPlatform(true);
				frame.add(new KMeans());
				frame.pack();
				frame.setVisible(true);
			}
		});
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(screenSize, screenSize);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

		for (int i = 0; i < clusterizedData.size(); i++) {
			g2.setColor(colors[i % colors.length]);
			g2.draw(new Ellipse2D.Double(clusters.get(i).getX(), clusters.get(i).getY(), 10, 10));

			for (int j = 0; j < clusterizedData.get(i).size(); j++) {
				g2.draw(new Ellipse2D.Double(clusterizedData.get(i).get(j).getX(), clusterizedData.get(i).get(j).getY(),
						5, 5));
			}
		}
	}

	private static void readData(String filename) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(filename)))) {
			String line;
			String delimiter = filename.equals("normal.txt") ? "	" : " ";
			double minX = Double.MAX_VALUE;
			double minY = Double.MAX_VALUE;
			double maxX = Double.MIN_VALUE;
			double maxY = Double.MIN_VALUE;

			while ((line = br.readLine()) != null) {
				String[] splitLine = line.split(delimiter);
				double x = Double.valueOf(splitLine[0]);
				double y = Double.valueOf(splitLine[1]);

				minX = x < minX ? x : minX;
				maxX = x > maxX ? x : maxX;
				minY = y < minY ? y : minY;
				maxY = y > maxY ? y : maxY;

				data.add(new Point(x, y));
			}

			inputDataMaxSize = maxX - minX > maxY - minY ? maxX - minX : maxY - minY;
			double minCoordinate = minX < minY ? minX : minY;

			for (Point point : data) {
				point.translate(-minCoordinate);
				point.scale(inputDataMaxSize, screenSize);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void createClusters() {
		for (int i = 0; i < clustersNumber; i++) {
			clusters.add(new Point(Math.random() * screenSize, Math.random() * screenSize));
		}
	}

	private static void clusterize() {
		clusterizedData = new ArrayList<>();

		for (int i = 0; i < clusters.size(); i++) {
			clusterizedData.add(new ArrayList<Point>());
		}

		for (int i = 0; i < data.size(); i++) {
			clusterizedData.get(closestClusterId(data.get(i))).add(data.get(i));
		}
	}

	private static int closestClusterId(Point point) {
		int closest = 0;

		for (int i = 0; i < clusters.size(); ++i) {
			closest = point.distanceTo(clusters.get(i)) < point.distanceTo(clusters.get(closest)) ? i : closest;
		}

		return closest;
	}

	private static boolean recalculateClusters() {
		int x;
		int y;
		boolean flag = false;

		for (int i = 0; i < clusters.size(); i++) {
			x = 0;
			y = 0;

			for (int j = 0; j < clusterizedData.get(i).size(); j++) {
				x += clusterizedData.get(i).get(j).getX();
				y += clusterizedData.get(i).get(j).getY();
			}
			if (clusterizedData.get(i).size() > 0) {
				if (x / clusterizedData.get(i).size() != clusters.get(i).getX()
						|| y / clusterizedData.get(i).size() != clusters.get(i).getY()) {
					flag = true;
				}
				clusters.get(i).setPosition(x / clusterizedData.get(i).size(), y / clusterizedData.get(i).size());
			}
		}

		return flag;
	}
}
