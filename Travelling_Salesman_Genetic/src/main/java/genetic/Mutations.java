package genetic;

public class Mutations {

	public static void swap(Individual individual, final double rate) {
		if (Math.random() < rate) {
			int firstGene = (int) (Math.random() * individual.getGenesSize());
			int secondGene = (int) (Math.random() * individual.getGenesSize());

			while (secondGene == firstGene) {
				secondGene = (int) (Math.random() * individual.getGenesSize());
			}
			
			individual.swapGenes(firstGene, secondGene);
		}
	}

}
