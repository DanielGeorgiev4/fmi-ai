package genetic;

public class TravellingSalesman {

	public static void main(String[] args) {
		int citiesNumber = Integer.valueOf(args[0]);
		int populationSize = 150;
		int numberOfGenerations = 500;
		double mutationRate = 0.01;
		Population population = new Population(citiesNumber, populationSize, mutationRate);

		int i = 0;
		for (; i < numberOfGenerations; i++) {
			if (population.bestIndividualFitness() == citiesNumber * 2 - 2) {
				break;
			}

			population.doSelection();
			population.doCrossover();

			if (i + 1 == 10) {
				printBestIndividual(i + 1, population.bestIndividualFitness());
			} else if ((i + 1) % 30 == 0) {
				printBestIndividual(i + 1, population.bestIndividualFitness());
			}
		}

		System.out.println("> final <" + i + "> generation -> " + population.bestIndividualFitness());
	}

	public static void printBestIndividual(final int generation, final int bestFitness) {
		System.out.println("> generation " + generation + " -> " + bestFitness);
	}

}