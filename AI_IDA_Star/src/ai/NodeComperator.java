package ai;

import java.util.Comparator;
import ai.Node;

public class NodeComperator<Node> implements Comparator<Node> {

	@Override
	public int compare(Node o1, Node o2) {
		return (Utils.manhattanDistance(((ai.Node) o1).getBoard()) + ((ai.Node) o1).getCost())
				- (Utils.manhattanDistance(((ai.Node) o2).getBoard()) + ((ai.Node) o2).getCost());
	}

}