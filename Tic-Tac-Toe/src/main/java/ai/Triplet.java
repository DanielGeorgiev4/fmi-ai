package ai;

public class Triplet {

	private int value;
	private int[] indexes = {-1,-1};

	public Triplet(int value, int[] indexes) {
		this.value = value;
		this.indexes[0] = indexes[0];
		this.indexes[1] = indexes[1];
	}

	public Triplet(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public int[] getIndexes() {
		return indexes;
	}

}
