package queens;

import java.util.ArrayList;
import java.util.List;

public class NQueens {

	static long start;
	static int size;
	static int[] board;
	static int[] rowConflicts;
	static int[] mainDiagonalConflicts;
	static int[] secondaryDiagonalConfilcts;

	public static void main(String[] args) {
		start = System.currentTimeMillis();
		init(args);

		while (true) {
			func();
			if (hasConflicts()) {
				init(args);
			} else {
				break;
			}
		}

		System.out.println("Time: " + (System.currentTimeMillis() - start));
		if (args.length < 2 || !"-q".equals(args[1])) {
			printBoard();
		}
	}

	private static void printBoard() {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				System.out.print(board[j] == i ? '*' : '_');
			}
			System.out.println();
		}
		System.out.println();
	}

	private static void func() {
		int col;
		int row;

		for (int i = 0; i < size * 3; i++) {
			col = getMaxConflicts();
			row = getMinConflicts(col);
			updateConflicts(col, board[col], row);
			board[col] = row;
			if (!hasConflicts()) {
				return;
			}
		}

		return;
	}

	private static void init(String[] args) {
		size = Integer.valueOf(args[0]);
		board = new int[size];
		rowConflicts = new int[size];
		mainDiagonalConflicts = new int[2 * size - 1];
		secondaryDiagonalConfilcts = new int[2 * size - 1];

		for (int i = 0; i < board.length; i++) {
			board[i] = (int) (Math.random() * size);
			rowConflicts[board[i]]++;
			mainDiagonalConflicts[mainDiaogalIndex(i, board[i])]++;
			secondaryDiagonalConfilcts[secondaryDiaogalIndex(i, board[i])]++;
		}
	}

	private static void updateConflicts(int x, int oldY, int newY) {
		rowConflicts[oldY]--;
		rowConflicts[newY]++;
		mainDiagonalConflicts[mainDiaogalIndex(x, oldY)]--;
		mainDiagonalConflicts[mainDiaogalIndex(x, newY)]++;
		secondaryDiagonalConfilcts[secondaryDiaogalIndex(x, oldY)]--;
		secondaryDiagonalConfilcts[secondaryDiaogalIndex(x, newY)]++;
	}

	private static boolean hasConflicts() {
		for (int i : rowConflicts) {
			if (i > 1) {
				return true;
			}
		}

		for (int i = 0; i < mainDiagonalConflicts.length; ++i) {
			if (mainDiagonalConflicts[i] > 1 || secondaryDiagonalConfilcts[i] > 1) {
				return true;
			}
		}

		return false;
	}

	private static int getMinConflicts(int x) {
		int minConflicts = Integer.MAX_VALUE;
		int conflicts;
		List<Integer> minConflictsIndexes = new ArrayList<>();

		for (int i = 0; i < board.length; i++) {
			conflicts = getConflicts(x, i);

			if (conflicts == minConflicts) {
				minConflictsIndexes.add(i);
			} else if (conflicts < minConflicts) {
				minConflicts = conflicts;
				minConflictsIndexes.clear();
				minConflictsIndexes.add(i);
			}
		}

		return minConflictsIndexes.get((int) (Math.random() * minConflictsIndexes.size()));
	}

	private static int getMaxConflicts() {
		int maxConflicts = 2;
		int conflicts;
		List<Integer> maxConflictsIndexes = new ArrayList<>();

		for (int i = 0; i < board.length; i++) {
			conflicts = getConflicts(i, board[i]);

			if (conflicts == maxConflicts) {
				maxConflictsIndexes.add(i);
			} else if (conflicts > maxConflicts) {
				maxConflicts = conflicts;
				maxConflictsIndexes.clear();
				maxConflictsIndexes.add(i);
			}
		}

		return maxConflictsIndexes.get((int) (Math.random() * maxConflictsIndexes.size()));
	}

	private static int getConflicts(int x, int y) {
		return rowConflicts[y] + mainDiagonalConflicts[mainDiaogalIndex(x, y)]
				+ secondaryDiagonalConfilcts[secondaryDiaogalIndex(x, y)];
	}

	private static int mainDiaogalIndex(int x, int y) {
		return x - y + size - 1;
	}

	private static int secondaryDiaogalIndex(int x, int y) {
		return x + y;
	}
}